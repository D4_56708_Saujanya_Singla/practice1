const { response } = require('express')
const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()


router.get('/abc',(request,response)=>{
    response.send('abc')
})


router.get('/',(request,response)=>{
    const statement = 'select * from emp'

db.execute(statement,(error,result)=>{
    response.send(utils.createResult(error,result));
})
})

module.exports= router