const express = require('express')
const emp = require('./routes/emp')
const app = express()

app.use(express.json())
app.use('/emp',emp)

app.listen(4000,'0.0.0.0',()=>{
    console.log('server started on port 4000')
})