create table emp(id int primary key auto_increment, name varchar(20), salary float, address varchar(100), age int);

insert into emp(name , salary, address,age) values('saujanya', 150000, 'haryana', 23);
insert into emp(name , salary, address,age) values('utkarsh', 100000, 'haryana', 23);
insert into emp(name , salary, address,age) values('ashwin', 20000, 'haryana', 23);
insert into emp(name , salary, address,age) values('dheeraj', 50000, 'haryana', 23);